# FluctuatingIntensities
This script is used to investigate the fluctuation of intensities of subresolution pixels from cell imaging using images from an OMERO server.

## Install
The scripts require Python>=3.6.10<br>
Please create a conda environment using<br>  
`$ conda env create -f FlucInte.yml `<br>
for running the jupyter notebooks<br>

For running FluctuatingIntensities.py please create a virtual environment with packages omero-py==5.6.9 cryptography and scikit-image
